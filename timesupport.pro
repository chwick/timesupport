TEMPLATE = lib
CONFIG += staticlib c++11 noqt
CONFIG -= qt

DESTDIR = $$shadowed($$PWD)

SOURCES += timesupport.cpp
HEADERS += timesupport.h
